package ch.bc;

public class GreetingsFromDocker {

  public static void main(String[] args) {
    boolean isEnough = false;
    int counter = 1;
    while (!isEnough){
      System.out.println("Greetings from docker!");
       counter = counter << 1;
      try {
        Thread.sleep(1024);
      } catch (InterruptedException ignore) {
        isEnough = true;
      }
      isEnough = isEnough | counter > 1023;
    }
  }
}
