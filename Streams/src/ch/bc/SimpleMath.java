package ch.bc;

import static java.lang.Math.sqrt;
import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public class SimpleMath {

  private static final int[] INPUT = new int[]{10, 87,97, 43, 121, 20, 225, 198};
  private static final int[] INPUT_FIB = new int[]{1,2,3,5,8,13};
  private static final int[] INPUT_COMPOSITE = new int[]{ 2,4,16};

  //NealFordFactorization
  private static IntStream factorize(int n){
    List<Integer> factors = range(1, (int) (sqrt(n) + 1))
        .filter(potential -> n % potential == 0)
        .boxed()
        .collect(toList());
    List<Integer> factorsAboveSqrt = factors
        .stream()
        .map(e -> n / e)
        .collect(toList());
    factors.addAll(factorsAboveSqrt);
    return factors.stream().mapToInt(i -> i).distinct();
  }

  public static void main(String[] args) {


    final int[] result =  Arrays
        .stream(INPUT)
        .flatMap(SimpleMath::factorize)
        .distinct()
        .sorted()
        .toArray();
    System.out.println(Arrays.toString(result));


    final int[] decomposedComposite =  Arrays
            .stream(INPUT_COMPOSITE)
            .flatMap(SimpleMath::factorize)
            .distinct()
            .sorted()
            .toArray();

    final int[] decomposed =  Arrays
            .stream(INPUT_FIB)
            .flatMap(SimpleMath::factorize)
            .distinct()
            .sorted()
            .toArray();


  }
}
