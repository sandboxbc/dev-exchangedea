package ch.bc.model;

/** Gets or Sets prescriptionStatus */
public enum PrescriptionStatus {
  DRAFT("draft"),

  ACTIVE("active"),

  ONHOLD("onHold"),

  COMPLETED("completed"),

  CANCELED("canceled");

  private final String value;

  PrescriptionStatus(String value) {
    this.value = value;
  }

  public static PrescriptionStatus fromValue(String value) {
    for (PrescriptionStatus b : PrescriptionStatus.values()) {
      if (b.value.equals(value)) {
        return b;
      }
    }
    throw new IllegalArgumentException("Unexpected value '" + value + "'");
  }

  public String getValue() {
    return value;
  }

  @Override
  public String toString() {
    return String.valueOf(value);
  }
}
