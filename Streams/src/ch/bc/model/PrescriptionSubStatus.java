package ch.bc.model;

/** Gets or Sets prescriptionSubStatus */
public enum PrescriptionSubStatus {
  NEW("new"),

  RELEASED("released"),

  EXPIRED("expired");

  private final String value;

  PrescriptionSubStatus(String value) {
    this.value = value;
  }

  public static PrescriptionSubStatus fromValue(String value) {
    for (PrescriptionSubStatus b : PrescriptionSubStatus.values()) {
      if (b.value.equals(value)) {
        return b;
      }
    }
    throw new IllegalArgumentException("Unexpected value '" + value + "'");
  }

  public String getValue() {
    return value;
  }

  @Override
  public String toString() {
    return String.valueOf(value);
  }
}
