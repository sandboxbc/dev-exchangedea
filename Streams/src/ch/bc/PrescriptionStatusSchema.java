package ch.bc;

import static ch.bc.model.PrescriptionStatus.CANCELED;
import static ch.bc.model.PrescriptionSubStatus.EXPIRED;
import static ch.bc.model.PrescriptionSubStatus.NEW;
import static ch.bc.model.PrescriptionSubStatus.RELEASED;

import ch.bc.model.PrescriptionStatus;
import ch.bc.model.PrescriptionSubStatus;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class PrescriptionStatusSchema {

  public static void main(String[] args) {
    final Map<String, List<String>> prescriptionSchema = createPrescriptionSchema();
    System.out.println(prescriptionSchema.values());
  }

  private static Map<String, List<String>> createPrescriptionSchema() {
    return Arrays.stream(PrescriptionStatus.values())
        .collect(
            Collectors.toMap(
                PrescriptionStatus::getValue,
                status ->
                    Arrays.stream(PrescriptionSubStatus.values())
                        .filter(
                            substatus ->
                                ((NEW == substatus || RELEASED == substatus)
                                        && PrescriptionStatus.ACTIVE == status)
                                    || (EXPIRED == substatus && CANCELED == status))
                        .map(PrescriptionSubStatus::getValue)
                        // Sorting is required to preserve the natural order of appearance of
                        // status values
                        .sorted(Comparator.comparing(PrescriptionSubStatus::fromValue))
                        .collect(Collectors.toList()),
                // Duplicate prescription statuses are not allowed
                // and based on the data type impossible
                // therefore merge operation reduced to "first come, first win" policy
                (status, duplicate) -> status,
                // Sorting guaranties appearance of the statuses in a logical order -
                // order of definition
                () -> new TreeMap<>(Comparator.comparing(PrescriptionStatus::fromValue))));
  }
}
