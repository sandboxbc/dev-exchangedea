FROM openjdk:8
COPY out/production/Docker/ch/bc /tmp
WORKDIR /tmp
ENTRYPOINT ["java","GreetingsFromDocker"]